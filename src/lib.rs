use maths_traits::{algebra, analysis};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Tense<T> {
	pub dim: Vec<u32>,
	val: Vec<T>,
}

impl<T> Tense<T> {
	/// ```rust
	/// # use tense::Tense;
	/// Tense::new(vec![3, 3, 2], vec![
	/// 	1, 2, 3,
	/// 	4, 5, 6,
	/// 	7, 8, 9,
	///
	/// 	1, 2, 3,
	/// 	4, 5, 6,
	/// 	7, 8, 9,
	/// ]);
	/// ```
	pub fn new(dim: Vec<u32>, val: Vec<T>) -> Self {
		Self { dim, val }
	}
	
	/// ```rust
	/// # use tense::Tense;
	/// assert_eq!(
	/// 	Tense::new(vec![3, 3], vec![
	/// 		1, 2, 3,
	/// 		4, 5, 6,
	/// 		7, 8, 9,
	/// 	]).rank(),
	/// 	2
	/// )
	/// ```
	pub fn rank(&self) -> usize {
		self.dim.len()
	}
	
	
	/// ```rust
	/// # use tense::Tense;
	/// assert!(
	/// 	Tense::new(vec![3, 2], vec![
	/// 		1, 2, 3,
	/// 		4, 5, 6,
	/// 	]).check()
	/// );
	/// assert!(
	/// 	!Tense::new(vec![3, 2], vec![
	/// 		1, 2, 3,
	/// 		4, 5,
	/// 	]).check()
	/// );
	/// ```
	pub fn check(&self) -> bool {
		self.dim.iter().product::<u32>() as usize == self.val.len()
	}
}

impl<T, R, O> algebra::group_like::additive::Add<Tense<R>> for Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Add<&'a R, Output = O>,
{
	type Output = Tense<O>;

	fn add(self, rhs: Tense<R>) -> Self::Output {
		Self::Output {
			dim: self.dim.clone(),
			val: self
				.val
				.iter()
				.zip(rhs.val.iter())
				.map(|(a, b)| a + b)
				.collect(),
		}
	}
}

impl<T, R, O> algebra::group_like::additive::Add<&Tense<R>> for Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Add<&'a R, Output = O>,
{
	type Output = Tense<O>;

	fn add(self, rhs: &Tense<R>) -> Self::Output {
		Self::Output {
			dim: self.dim.clone(),
			val: self
				.val
				.iter()
				.zip(rhs.val.iter())
				.map(|(a, b)| a + b)
				.collect(),
		}
	}
}

impl<T, R, O> algebra::group_like::additive::Add<Tense<R>> for &Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Add<&'a R, Output = O>,
{
	type Output = Tense<O>;

	fn add(self, rhs: Tense<R>) -> Self::Output {
		Self::Output {
			dim: self.dim.clone(),
			val: self
				.val
				.iter()
				.zip(rhs.val.iter())
				.map(|(a, b)| a + b)
				.collect(),
		}
	}
}

impl<T, R, O> algebra::group_like::additive::Add<&Tense<R>> for &Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Add<&'a R, Output = O>,
{
	type Output = Tense<O>;

	fn add(self, rhs: &Tense<R>) -> Self::Output {
		Self::Output {
			dim: self.dim.clone(),
			val: self
				.val
				.iter()
				.zip(rhs.val.iter())
				.map(|(a, b)| a + b)
				.collect(),
		}
	}
}

impl<T, R> algebra::group_like::additive::AddAssign<Tense<R>> for Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Add<&'a R, Output = T>,
{
	fn add_assign(&mut self, rhs: Tense<R>) {
		self.val = self
			.val
			.iter()
			.zip(rhs.val.iter())
			.map(|(a, b)| a + b)
			.collect();
	}
}

impl<T, R> algebra::group_like::additive::AddAssign<&Tense<R>> for Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Add<&'a R, Output = T>,
{
	fn add_assign(&mut self, rhs: &Tense<R>) {
		self.val = self
			.val
			.iter()
			.zip(rhs.val.iter())
			.map(|(a, b)| a + b)
			.collect();
	}
}

impl<T, R, O> algebra::group_like::additive::Sub<Tense<R>> for Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Sub<&'a R, Output = O>,
{
	type Output = Tense<O>;

	fn sub(self, rhs: Tense<R>) -> Self::Output {
		Self::Output {
			dim: self.dim.clone(),
			val: self
				.val
				.iter()
				.zip(rhs.val.iter())
				.map(|(a, b)| a - b)
				.collect(),
		}
	}
}

impl<T, R, O> algebra::group_like::additive::Sub<&Tense<R>> for Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Sub<&'a R, Output = O>,
{
	type Output = Tense<O>;

	fn sub(self, rhs: &Tense<R>) -> Self::Output {
		Self::Output {
			dim: self.dim.clone(),
			val: self
				.val
				.iter()
				.zip(rhs.val.iter())
				.map(|(a, b)| a - b)
				.collect(),
		}
	}
}

impl<T, R, O> algebra::group_like::additive::Sub<Tense<R>> for &Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Sub<&'a R, Output = O>,
{
	type Output = Tense<O>;

	fn sub(self, rhs: Tense<R>) -> Self::Output {
		Self::Output {
			dim: self.dim.clone(),
			val: self
				.val
				.iter()
				.zip(rhs.val.iter())
				.map(|(a, b)| a - b)
				.collect(),
		}
	}
}

impl<T, R, O> algebra::group_like::additive::Sub<&Tense<R>> for &Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Sub<&'a R, Output = O>,
{
	type Output = Tense<O>;

	fn sub(self, rhs: &Tense<R>) -> Self::Output {
		Self::Output {
			dim: self.dim.clone(),
			val: self
				.val
				.iter()
				.zip(rhs.val.iter())
				.map(|(a, b)| a - b)
				.collect(),
		}
	}
}

impl<T, R> algebra::group_like::additive::SubAssign<Tense<R>> for Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Sub<&'a R, Output = T>,
{
	fn sub_assign(&mut self, rhs: Tense<R>) {
		self.val = self
			.val
			.iter()
			.zip(rhs.val.iter())
			.map(|(a, b)| a - b)
			.collect();
	}
}

impl<T, R> algebra::group_like::additive::SubAssign<&Tense<R>> for Tense<T>
where
	for<'a> &'a T: algebra::group_like::additive::Sub<&'a R, Output = T>,
{
	fn sub_assign(&mut self, rhs: &Tense<R>) {
		self.val = self
			.val
			.iter()
			.zip(rhs.val.iter())
			.map(|(a, b)| a - b)
			.collect();
	}
}

impl<T, O> algebra::module_like::Neg for Tense<T>
where
	for<'a> &'a T: algebra::module_like::Neg<Output = O>,
{
	type Output = Tense<O>;
	
	fn neg(self) -> Self::Output {
		Self::Output {
			dim: self.dim,
			val: self.val.iter().map(|a| a.neg()).collect()
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_add() {
		let mut a = Tense::new(vec![2], vec![100, 200]);
		let b = Tense::new(vec![2], vec![10, 20]);
		let c = Tense::new(vec![2], vec![110, 220]);
		assert_eq!(&a + &b, c);
		assert_eq!(a.clone() + &b, c);
		assert_eq!(&a + b.clone(), c);
		a += &b;
		a += b.clone();
		assert_eq!(a, b + c);
	}

	#[test]
	fn test_sub() {
		let mut a = Tense::new(vec![2], vec![110, 220]);
		let b = Tense::new(vec![2], vec![10, 20]);
		let c = Tense::new(vec![2], vec![100, 200]);
		assert_eq!(&a - &b, c);
		assert_eq!(a.clone() - &b, c);
		assert_eq!(&a - b.clone(), c);
		a -= &b;
		a -= b.clone();
		assert_eq!(a, c - b);
	}
	
	#[test]
	fn test_neg() {
		let a = Tense::new(vec![2], vec![1, 2]);
		let b = Tense::new(vec![2], vec![-1, -2]);
		assert_eq!(-a, b);
	}
}
