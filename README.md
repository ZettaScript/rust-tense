# Tense

This crate provides traits, struct, macro for n-rank tensor calculus. It can handle tensors such as scalars, vectors, matrices, etc.

Under development: tensors' mathematical properties and operators not yet completely and rigorously implemented. API may break over time.

	assert_eq!(
		tense!(tense!(0, 1, 4), tense!(42, 12, 7), tense!(10, 20, -100))
			+ tense!(tense!(5, 2, 4), tense!(42, 12, 7), tense!(10, 20, -100))
			+ tense!(tense!(1000)),
		tense!(
			tense!(1005, 1003, 1008),
			tense!(1084, 1024, 1014),
			tense!(1020, 1040, 800)
		)
	);

## Why this crate?

There is a lot of crates about vectors or matrices, but vectors and matrices are special cases of tensors (respectively of ranks 1 and 2). There are also a lot of crates about tensors, but each time for a very paticular purpose. This crate aims to be general, so it can handle any type implementing `maths_traits`, furthermore all your tensors can interoperate.

## Quick use

A `Tense<T>` struct represents a vector of _n_ coordinates of type `T`. Since a matrix can be represented as a vector containing vectors, it is written as nested `Tense` instances.

	assert_eq!(
		tense!(1, 2, 3).val,
		vec![1, 2, 3],
	);
	assert_eq!(
		tense!(
			tense!(4, 0),
			tense!(7, 4),
		).norm(2),
		9,
	);
